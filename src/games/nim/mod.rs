mod game;
mod action_iterator;

use crate::games::{ActionTrait, SpecificStateTrait};

pub use self::game::NimGame;
pub use self::action_iterator::NimActionIterator;


#[derive(Clone, Debug, Eq, Hash, PartialEq, Serialize)]
pub struct NimState {
    pub pegs: Vec<i32>,
}


impl SpecificStateTrait for NimState {}


#[derive(Clone, Debug, Serialize)]
#[serde(tag = "type")]
pub enum NimActions {
    Take{columns: Vec<usize>, count: i32},
}


impl ActionTrait for NimActions {}
