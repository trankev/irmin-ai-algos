use super::{NimState, NimActions};
use crate::games::{GameRules, GameState, GameStatus};


pub struct NimGame {
    pub pegs: Vec<i32>,
    pub take_max_columns: usize,
    pub take_max_count: i32,
    last_take_wins: bool,
}


impl NimGame {
    pub fn new(pegs: Vec<i32>, mut take_max_columns: usize, mut take_max_count: i32, last_take_wins: bool) -> NimGame {
        if take_max_columns == 0 {
            take_max_columns = pegs.len();
        }
        if take_max_count == 0 {
            take_max_count = *pegs.iter().max().unwrap();
        }
        NimGame {
            pegs,
            take_max_columns,
            take_max_count,
            last_take_wins,
        }
    }
}


impl GameRules for NimGame {
    type SpecificState = NimState;
    type Action = NimActions;

    fn get_initial_state(&self) -> GameState<NimState> {
        GameState {
            specific: NimState {
                pegs: self.pegs.clone(),
            },
            current_player: 0,
        }
    }

    fn compute_status(&self, game_state: &GameState<NimState>) -> GameStatus {
        if game_state.specific.pegs.iter().any(|x| *x > 0) {
            return GameStatus::Ongoing;
        }
        if self.last_take_wins {
            return GameStatus::Winner(1 - game_state.current_player);
        }
        return GameStatus::Winner(game_state.current_player);
    }

    fn play_action(&self, game_state: &GameState<NimState>, action: &NimActions) -> GameState<NimState> {
        match action {
            NimActions::Take{columns, count} => {
                let mut new_pegs = game_state.specific.pegs.clone();
                for &column in columns {
                    new_pegs[column] -= count;
                }
                GameState {
                    specific: NimState {
                        pegs: new_pegs,
                    },
                    current_player: 1 - game_state.current_player,
                }
            }
        }
    }
}
