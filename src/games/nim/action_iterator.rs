use super::{NimActions, NimGame, NimState};

use crate::utils::combinations;
use crate::games::{GameState, ActionIterator};


pub struct NimActionIterator {
    combination: combinations::CombinationIterator,
    columns: Vec<usize>,
    current_count: i32,
    max_count: i32,
    total_max_count: i32,
    pegs: Vec<i32>,
}


impl ActionIterator<NimGame> for NimActionIterator {
    fn new(game: &NimGame, game_state: &GameState<NimState>) -> NimActionIterator {
        NimActionIterator {
            combination: combinations::CombinationIterator::new(game.pegs.len(), 1, game.take_max_columns),
            current_count: 0,
            columns: vec![],
            max_count: 0,
            total_max_count: game.take_max_count,
            pegs: game_state.specific.pegs.clone(),
        }
    }
}

impl Iterator for NimActionIterator {
    type Item = NimActions;

    fn next(&mut self) -> Option<NimActions> {
        self.current_count += 1;
        if self.current_count > self.max_count {
            self.current_count = 1;
            loop {
                match self.combination.next() {
                    Some(columns) => {
                        self.columns = columns;
                        let column_pegs = self.columns.iter().map(|index| self.pegs[*index]);
                        self.max_count = match column_pegs.min() {
                            Some(max_count) => std::cmp::min(max_count, self.total_max_count),
                            None => { continue; },
                        };
                        if self.max_count > 0 {
                            break;
                        }
                    }
                    None => { return None; },
                }
            }
        }
        let next_move = NimActions::Take{columns: self.columns.clone(), count: self.current_count};
        return Some(next_move);
    }
}
