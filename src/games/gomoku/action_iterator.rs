use super::{GomokuActions, GomokuGame, GomokuState};
use crate::games::{ActionIterator, PlayerId};
use crate::utils::vectors::indices;


pub struct GomokuActionIterator {
    dimensions: Vec<isize>,
    grid: Vec<PlayerId>,
    current_index: usize,
}


impl ActionIterator<GomokuGame> for GomokuActionIterator {
    fn new(game: &GomokuGame, game_state: &GomokuState) -> GomokuActionIterator {
        GomokuActionIterator {
            dimensions: game.dimensions.clone(),
            grid: game_state.specific.grid.clone(),
            current_index: game_state.specific.grid.len(),
        }
    }
}

impl Iterator for GomokuActionIterator {
    type Item = GomokuActions;

    fn next(&mut self) -> Option<GomokuActions> {
        loop {
            if self.current_index == 0 {
                return None;
            }
            self.current_index -= 1;
            if self.grid[self.current_index] == -1 {
                let position = indices::index_to_vector(&self.dimensions, self.current_index);
                return Some(GomokuActions::Place{position});
            }
        }
    }
}
