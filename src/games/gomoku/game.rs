use super::{GomokuSpecifics, GomokuState, GomokuActions};
use crate::games::{GameRules, GameState, GameStatus, PlayerId};
use crate::utils::vectors::indices;
use crate::utils::vectors::strips::StripIterator;


pub struct GomokuGame {
    pub dimensions: Vec<isize>,
    series_size: usize,
    allow_overlines: bool,
    strips: Vec<Vec<usize>>,
}


impl GomokuGame {
    pub fn new(dimensions: Vec<isize>, series_size: usize, allow_overlines: bool) -> GomokuGame {
        let strips = StripIterator::new(&dimensions, series_size).collect();
        GomokuGame {
            dimensions,
            series_size,
            allow_overlines,
            strips,
        }
    }

    fn check_count_status(&self, count: usize, player: PlayerId) -> Option<PlayerId> {
        if count == self.series_size {
            return Some(player);
        } else if count > self.series_size {
            if self.allow_overlines {
                return Some(player);
            }
            return Some(1 - player);
        }
        return None;
    }

    fn compute_strip_status(&self, state: &GomokuSpecifics, strip: &Vec<usize>) -> GameStatus {
        let mut previous_value = -1;
        let mut current_entity_count = 0;
        let mut last_player = -1;
        let mut last_player_potential = 0;
        let mut can_continue = false;

        for index in strip {
            let value = state.grid[*index];
            if value != previous_value {
                if previous_value > -1 {
                    if let Some(winner) = self.check_count_status(current_entity_count, last_player) {
                        return GameStatus::Winner(winner);
                    }
                }
                if value > -1 {
                    if last_player != value {  // ox or o.x
                        if last_player_potential >= self.series_size {
                            can_continue = true;
                        }
                        last_player_potential = 0;
                        if previous_value == -1 {  // ..x
                            last_player_potential = current_entity_count;
                        }
                    }
                    last_player = value;
                }
                current_entity_count = 0;
                previous_value = value;
            }
            last_player_potential += 1;
            current_entity_count += 1;
        }
        if previous_value > -1 {
            if let Some(winner) = self.check_count_status(current_entity_count, last_player) {
                return GameStatus::Winner(winner);
            }
        }
        if last_player_potential >= self.series_size {
            can_continue = true;
        }
        if can_continue { GameStatus::Ongoing } else { GameStatus::Draw }
    }
}


impl GameRules for GomokuGame {
    type SpecificState = GomokuSpecifics;
    type Action = GomokuActions;

    fn get_initial_state(&self) -> GomokuState {
        let grid_size: isize = self.dimensions.iter().product();
        GameState {
            specific: GomokuSpecifics {
                grid: vec![-1; grid_size as usize],
            },
            current_player: 0,
        }
    }

    fn compute_status(&self, game_state: &GomokuState) -> GameStatus {
        let mut can_continue = false;
        for strip in &self.strips {
            match self.compute_strip_status(&game_state.specific, &strip) {
                GameStatus::Winner(player) => { return GameStatus::Winner(player); },
                GameStatus::Ongoing => { can_continue = true; },
                _ => (),
            }
        }
        if can_continue { GameStatus::Ongoing } else { GameStatus::Draw }
    }

    fn play_action(&self, game_state: &GomokuState, action: &GomokuActions) -> GomokuState {
        match action {
            GomokuActions::Place{position} => {
                let mut result = game_state.clone();
                result.specific.grid[indices::vector_to_index(&self.dimensions, &position)] = result.current_player;
                result.current_player = 1 - result.current_player;
                return result;
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::{GomokuActions, GomokuGame, GomokuSpecifics};
    use crate::games::{GameRules, GameState, GameStatus};

    #[test]
    fn initial_state() {
        let game = GomokuGame::new(vec![3, 3], 3, false);
        let result = game.get_initial_state();
        let expected = GameState { specific: GomokuSpecifics { grid: vec![-1; 9] }, current_player: 0};
        assert_eq!(result, expected);
    }

    #[test]
    fn play_action() {
        let game = GomokuGame::new(vec![3, 3], 3, false);
        let game_state = game.get_initial_state();
        let action = GomokuActions::Place{position: vec![2, 1]};
        let resulting_state = game.play_action(&game_state, &action);
        let expected = GameState {
            current_player: 1,
            specific: GomokuSpecifics {
                grid: vec![
                    -1, -1, -1,
                    -1, -1, -1,
                    -1,  0, -1
                ],
            }
        };
        assert_eq!(resulting_state, expected);
    }

    macro_rules! compute_status_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (grid, expected_status) = $value;
                let game = GomokuGame::new(vec![3, 3], 3, false);
                let game_state = GameState {
                    current_player: 0,
                    specific: GomokuSpecifics {
                        grid: grid,
                    }
                };
                let status = game.compute_status(&game_state);
                assert_eq!(status, expected_status);
            }
        )*
        }
    }

    compute_status_tests! {
        test_compute_status_start: (
            vec![-1; 9],
            GameStatus::Ongoing,
        ),
        test_compute_status_ongoing: (
            vec![
                 0, -1, -1,
                 1,  0,  0,
                 1,  0,  1,
            ],
            GameStatus::Ongoing,
        ),
        test_compute_status_win: (
            vec![
                 0, -1, -1,
                 0,  0,  1,
                 0,  1,  1,
            ],
            GameStatus::Winner(0),
        ),
        test_compute_status_draw: (
            vec![
                 0,  1, -1,
                 1,  0,  0,
                 1,  0,  1,
            ],
            GameStatus::Draw,
        ),
    }

    macro_rules! compute_strip_status_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (grid, expected_status) = $value;
                let game = GomokuGame::new(vec![5], 3, false);
                let game_state =  GomokuSpecifics {
                    grid: grid,
                };
                let direction = vec![0, 1, 2, 3, 4];
                let status = game.compute_strip_status(&game_state, &direction);
                assert_eq!(status, expected_status);
            }
        )*
        }
    }

    compute_strip_status_tests! {
        test_compute_strip_status_ongoing_empty: (
            vec![-1; 5],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_ongoing_at_start: (
            vec![-1, -1, 0, 1, 1],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_ongoing_at_end: (
            vec![1, 1, 0, -1, -1],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_ongoing_middle_begin: (
            vec![1, 0, -1, -1, 1],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_ongoing_middle_middle: (
            vec![1, -1, 0, -1, 1],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_ongoing_middle_end: (
            vec![1, -1, -1, 0, 1],
            GameStatus::Ongoing,
        ),
        test_compute_strip_status_draw: (
            vec![1, -1, 0, 1, -1],
            GameStatus::Draw,
        ),
        test_compute_strip_status_winner_start: (
            vec![0, 0, 0, 1, 0],
            GameStatus::Winner(0),
        ),
        test_compute_strip_status_winner_middle: (
            vec![1, 0, 0, 0, 1],
            GameStatus::Winner(0),
        ),
        test_compute_strip_status_winner_end: (
            vec![1, 1, 0, 0, 0],
            GameStatus::Winner(0),
        ),
    }
}
