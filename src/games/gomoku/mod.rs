mod action_iterator;
mod game;

use crate::games::{ActionTrait, PlayerId, SpecificStateTrait, GameState};

pub use self::game::GomokuGame;
pub use self::action_iterator::GomokuActionIterator;


#[derive(Clone, Debug, Eq, Hash, PartialEq, Serialize)]
pub struct GomokuSpecifics {
    pub grid: Vec<PlayerId>,
}


impl SpecificStateTrait for GomokuSpecifics {}


type GomokuState = GameState<GomokuSpecifics>;


#[derive(Clone, Debug, Serialize)]
#[serde(tag = "type")]
pub enum GomokuActions {
    Place{position: Vec<isize>},
}


impl ActionTrait for GomokuActions {}
