mod game;

pub mod gomoku;
pub mod nim;


pub use self::game::{
    ActionTrait,
    ActionIterator,
    GameRules,
    GameState,
    GameStatus,
    PlayerId,
    SpecificStateTrait,
};
