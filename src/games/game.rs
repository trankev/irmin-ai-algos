use serde::Serialize;


#[derive(Debug, PartialEq)]
pub enum GameStatus {
    Ongoing,
    Winner(PlayerId),
    Draw,
}


pub trait SpecificStateTrait: Clone + std::fmt::Debug + Eq + std::hash::Hash + PartialEq + Serialize {}
pub trait ActionTrait: Clone + std::fmt::Debug + Serialize {}
pub type PlayerId = i8;


#[derive(Clone, Debug, Eq, Hash, PartialEq, Serialize)]
pub struct GameState<SpecificState> where
    SpecificState: SpecificStateTrait,
{
    pub specific: SpecificState,
    pub current_player: PlayerId,
}


pub trait GameRules {
    type SpecificState: SpecificStateTrait;
    type Action: ActionTrait;

    fn get_initial_state(&self) -> GameState<Self::SpecificState>;
    fn compute_status(&self, game_state: &GameState<Self::SpecificState>) -> GameStatus;
    fn play_action(&self, game_state: &GameState<Self::SpecificState>, action: &Self::Action) -> GameState<Self::SpecificState>;
}


pub trait ActionIterator<Game>: Iterator<Item=Game::Action> where
    Game: GameRules,
{
    fn new(game: &Game, game_state: &GameState<<Game as GameRules>::SpecificState>) -> Self;
}
