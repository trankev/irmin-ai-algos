pub struct CombinationIterator {
    count: usize,
    end_state: isize,
    state: isize,
    min_size: usize,
    max_size: usize,
}


impl CombinationIterator {
    pub fn new(count: usize, min_size: usize, max_size: usize) -> CombinationIterator {
        CombinationIterator {
            count: count,
            end_state: 2_isize.pow(count as u32),
            state: -1,
            min_size: min_size,
            max_size: max_size,
        }
    }
}


fn count_set_bits(mut number: isize) -> usize {
    let mut result = 0;
    while number > 0 {
        if number & 0b1 != 0 {
            result += 1;
        }
        number >>= 1;
    }
    return result;
}


impl std::iter::Iterator for CombinationIterator {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Vec<usize>> {
        loop {
            self.state += 1;
            if self.state >= self.end_state {
                return None;
            }
            let column_count = count_set_bits(self.state);
            if column_count >= self.min_size && column_count <= self.max_size {
                break;
            }
        }
        let indices = (0..self.count).filter(|x| self.state & (1_isize << x) != 0).collect();
        return Some(indices);
    }
}
