use crate::utils::combinations::CombinationIterator;


pub struct StripStarts {
    dimensions: Vec<isize>,
    direction: Vec<isize>,
    origin: Vec<isize>,
    current_point: Vec<isize>,
    move_direction: Vec<isize>,
    current_combination: Vec<usize>,
    combination_iterator: CombinationIterator,
}


impl StripStarts {
    pub fn new(dimensions: &Vec<isize>, direction: &Vec<isize>) -> StripStarts {
        let mut origin = vec![0; dimensions.len()];
        let mut move_direction = vec![1; dimensions.len()];
        for (index, coordinate) in direction.iter().enumerate() {
            if *coordinate < 0 {
                origin[index] = dimensions[index] - 1;
                move_direction[index] = -1;
            }
        }
        let current_point = origin.clone();
        let combination_iterator = CombinationIterator::new(dimensions.len(), 0, dimensions.len() - 1);
        StripStarts {
            dimensions: dimensions.clone(),
            direction: direction.clone(),
            origin: origin,
            current_point: current_point,
            move_direction: move_direction,
            current_combination: Vec::new(),
            combination_iterator: combination_iterator,

        }
    }
}


impl Iterator for StripStarts {
    type Item = Vec<isize>;

    fn next(&mut self) -> Option<Vec<isize>> {
        for item in &self.current_combination {
            let index = *item;
            self.current_point[index] += self.move_direction[index];
            if self.current_point[index] >= 0 && self.current_point[index] < self.dimensions[index] {
                return Some(self.current_point.clone());
            }
            self.current_point[index] = self.origin[index] + self.move_direction[index];
        }
        loop {
            if let Some(combination) = self.combination_iterator.next() {
                self.current_combination = combination;
            } else {
                return None;
            }
            let mut non_indexed = self.direction.iter().enumerate().filter(
                |(index, _value)| !self.current_combination.contains(&index)
            );
            if non_indexed.any(|(_index, &value)| value != 0) {
                self.current_point = self.origin.clone();
                for index in &self.current_combination {
                    self.current_point[*index] += self.move_direction[*index];
                }
                break;
            }
        }
        return Some(self.current_point.clone());
    }
}


#[cfg(test)]
mod tests {
    use super::StripStarts;

    macro_rules! strip_start_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (dimensions, direction, expected) = $value;
                let starts: Vec<Vec<isize>> = StripStarts::new(&dimensions, &direction).collect();
                assert_eq!(
                    starts.len(),
                    expected.len(),
                    "Result different from expected: \n   {:?}\n!= {:?}\n",
                    starts,
                    expected,
                );
                for item in &expected {
                    assert!(
                        starts.contains(item),
                        "Result different from expected: \n   {:?}\n!= {:?}\n",
                        starts,
                        expected,
                    );
                }
            }
        )*
        }
    }

    strip_start_tests! {
        test_1d: (
            vec![2],
            vec![1],
            vec![
                vec![0],
            ],
        ),
        test_2d_horizontal: (
            vec![3, 4],
            vec![0, 1],
            vec![
                vec![0, 0],
                vec![1, 0],
                vec![2, 0],
            ],
        ),
        test_2d_vertical: (
            vec![3, 4],
            vec![1, 0],
            vec![
                vec![0, 0],
                vec![0, 1],
                vec![0, 2],
                vec![0, 3],
            ],
        ),
        test_2d_diagonal: (
            vec![3, 4],
            vec![1, 1],
            vec![
                vec![0, 0],
                vec![1, 0],
                vec![2, 0],
                vec![0, 1],
                vec![0, 2],
                vec![0, 3],
            ],
        ),
        test_2d_reverse: (
            vec![3, 4],
            vec![1, -1],
            vec![
                vec![0, 0],
                vec![0, 1],
                vec![0, 2],
                vec![0, 3],
                vec![1, 3],
                vec![2, 3],
            ],
        ),
        test_3d_linear_x: (
            vec![2; 3],
            vec![1, 0, 0],
            vec![
                vec![0, 0, 0],
                vec![0, 1, 0],
                vec![0, 0, 1],
                vec![0, 1, 1],
            ],
        ),
        test_3d_linear_y: (
            vec![2; 3],
            vec![0, 1, 0],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 0, 1],
                vec![1, 0, 1],
            ],
        ),
        test_3d_linear_z: (
            vec![2; 3],
            vec![0, 0, 1],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 1, 0],
                vec![1, 1, 0],
            ],
        ),
        test_3d_diagonal_xy: (
            vec![2; 3],
            vec![1, 1, 0],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 1, 0],

                vec![0, 0, 1],
                vec![1, 0, 1],
                vec![0, 1, 1],
            ],
        ),
        test_3d_diagonal_xz: (
            vec![2; 3],
            vec![1, 0, 1],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 1, 0],
                vec![1, 1, 0],

                vec![0, 0, 1],
                vec![0, 1, 1],
            ],
        ),
        test_3d_diagonal_yz: (
            vec![2; 3],
            vec![0, 1, 1],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 1, 0],
                vec![1, 1, 0],

                vec![0, 0, 1],
                vec![1, 0, 1],
            ],
        ),
        test_3d_diagonal_xyz: (
            vec![2; 3],
            vec![1, 1, 1],
            vec![
                vec![0, 0, 0],
                vec![1, 0, 0],
                vec![0, 1, 0],
                vec![1, 1, 0],

                vec![0, 0, 1],
                vec![1, 0, 1],
                vec![0, 1, 1],
            ],
        ),
    }
}
