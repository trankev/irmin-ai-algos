pub fn vector_to_index(dimensions: &Vec<isize>, vector: &Vec<isize>) -> usize {
    let result = dimensions.iter().zip(vector).fold(0, |acc, (dimension, value)| acc * dimension + value);
    result as usize
}


pub fn index_to_vector(dimensions: &Vec<isize>, value: usize) -> Vec<isize> {
    let value = value as isize;
    let mut result: Vec<isize> = dimensions.iter().rev().scan(value, |index, &dimension| {
        let value = *index % dimension;
        *index /= dimension;
        Some(value)
    }).collect();
    result.reverse();
    return result;
}


pub fn is_within_grid(dimensions: &Vec<isize>, vector: &Vec<isize>) -> bool {
    vector.iter().zip(dimensions.iter()).all(
        |(&coordinate, &max_bound)| coordinate >= 0 && coordinate < max_bound
    )
}


pub fn add_vectors(vector_a: &Vec<isize>, vector_b: &Vec<isize>) -> Vec<isize> {
    vector_a.iter().zip(vector_b.iter()).map(|(i, j)| i + j).collect()
}


#[cfg(test)]
mod tests {
    use super::{index_to_vector, vector_to_index};

    macro_rules! vector_to_index_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (dimensions, vector, expected) = $value;
                let index = vector_to_index(&dimensions, &vector);
                assert_eq!(index, expected);
                let vectorized = index_to_vector(&dimensions, index);
                assert_eq!(vectorized, vector);
            }
        )*
        }
    }

    vector_to_index_tests! {
        vec_0: (vec![3, 3], vec![0, 0], 0),
        vec_2: (vec![3, 3], vec![0, 2], 2),
        vec_1: (vec![3, 3], vec![2, 0], 6),
        vec_3: (vec![3, 3], vec![2, 2], 8),
        vec_4: (vec![2, 3, 5, 7], vec![1, 2, 3, 4], 200),
    }
}
