use super::directions::Directions;
use super::strip_starts::StripStarts;
use super::indices;


pub struct StripIterator {
    min_size: usize,
    dimensions: Vec<isize>,
    direction_iterator: Directions,
    current_direction: Vec<isize>,
    strip_start_iterator: StripStarts,
}


impl StripIterator {
    pub fn new(dimensions: &Vec<isize>, min_size: usize) -> StripIterator {
        let mut direction_iterator = Directions::new(dimensions.len());
        let current_direction = direction_iterator.next().unwrap();
        let strip_start_iterator = StripStarts::new(&dimensions, &current_direction);
        StripIterator {
            min_size: min_size,
            dimensions: dimensions.clone(),
            direction_iterator: direction_iterator,
            current_direction: current_direction,
            strip_start_iterator: strip_start_iterator,
        }
    }
}


impl Iterator for StripIterator {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(mut position) = self.strip_start_iterator.next() {
                let mut result = Vec::new();
                loop {
                    result.push(indices::vector_to_index(&self.dimensions, &position));
                    position = indices::add_vectors(&position, &self.current_direction);
                    if !indices::is_within_grid(&self.dimensions, &position) {
                        break;
                    }
                }
                if result.len() < self.min_size {
                    continue;
                }
                return Some(result);
            }
            if let Some(direction) = self.direction_iterator.next() {
                self.current_direction = direction;
                self.strip_start_iterator = StripStarts::new(&self.dimensions, &self.current_direction);
            } else {
                return None;
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::StripIterator;

    #[test]
    fn test_iterate_strips() {
        let dimensions = vec![3, 3];
        let result: Vec<Vec<usize>> = StripIterator::new(&dimensions, 2).collect();
        let expected = vec![
            vec![0, 1, 2],
            vec![3, 4, 5],
            vec![6, 7, 8],

            vec![0, 3, 6],
            vec![1, 4, 7],
            vec![2, 5, 8],

            vec![0, 4, 8],
            vec![1, 5],
            vec![3, 7],

            vec![2, 4, 6],
            vec![1, 3],
            vec![5, 7],
        ];

        assert_eq!(
            result.len(),
            expected.len(),
            "Result different from expected: \n   {:?}\n!= {:?}\n",
            result,
            expected,
        );
        for item in &expected {
            assert!(
                result.contains(item),
                "Result different from expected: \n   {:?}\n!= {:?}\n",
                result,
                expected,
            );
        }
    }
}
