pub struct Directions {
    current: Vec<isize>,
}


impl Directions {
    pub fn new(size: usize) -> Directions {
        Directions {
            current: vec![0; size],
        }
    }
}


impl Iterator for Directions {
    type Item = Vec<isize>;

    fn next(&mut self) -> Option<Vec<isize>> {
        for coordinate in self.current.iter_mut().rev() {
            *coordinate += 1;
            if *coordinate > 1 {
                *coordinate = -1;
            } else {
                return Some(self.current.clone());
            }
        }
        return None;
    }
}


#[cfg(test)]
mod tests {
    use super::Directions;

    #[test]
    fn test_one_dimension() {
        let result: Vec<Vec<isize>> = Directions::new(1).collect();
        let expected = vec![vec![1]];
        assert_eq!(result, expected);
    }

    #[test]
    fn test_two_dimensions() {
        let result: Vec<Vec<isize>> = Directions::new(2).collect();
        let expected = vec![
            vec![0, 1],
            vec![1, -1],
            vec![1, 0],
            vec![1, 1],
        ];
        assert_eq!(result, expected);
    }

    #[test]
    fn test_three_dimensions() {
        let result: Vec<Vec<isize>> = Directions::new(3).collect();
        let expected = vec![
            vec![0, 0, 1],

            vec![0, 1, -1],
            vec![0, 1, 0],
            vec![0, 1, 1],

            vec![1, -1, -1],
            vec![1, -1, 0],
            vec![1, -1, 1],

            vec![1, 0, -1],
            vec![1, 0, 0],
            vec![1, 0, 1],

            vec![1, 1, -1],
            vec![1, 1, 0],
            vec![1, 1, 1],
        ];
        assert_eq!(result, expected);
    }
}
