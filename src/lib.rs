#[macro_use] extern crate serde_derive;

pub mod algorithms;
pub mod games;
pub mod utils;
