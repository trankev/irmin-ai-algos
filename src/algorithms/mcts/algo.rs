use rand::seq::SliceRandom;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;

use crate::algorithms::{ActionEvaluation, Policy};
use crate::games::{ActionIterator, GameRules, GameState, GameStatus, PlayerId};
use super::MCTSState;


type NodeIndex = petgraph::graph::NodeIndex<u32>;
type EdgeIndex = petgraph::graph::EdgeIndex<u32>;


pub struct MCTS<Game, MoveIterator> where
    Game: GameRules,
    MoveIterator: ActionIterator<Game>,
{
    game: Game,
    exploration_factor: f32,
    root_node: NodeIndex,
    graph: petgraph::Graph<MCTSState, ()>,
    state_mapping: bimap::BiMap<NodeIndex, GameState<Game::SpecificState>>,
    action_mapping: HashMap<EdgeIndex, Game::Action>,
    phantom: std::marker::PhantomData<MoveIterator>,
}


impl<Game, MoveIterator> MCTS<Game, MoveIterator> where
    Game: GameRules,
    MoveIterator: ActionIterator<Game>,
{
    pub fn new(game: Game) -> MCTS<Game, MoveIterator> {
        MCTS {
            game: game,
            exploration_factor: 2.0f32.sqrt(),
            graph: petgraph::Graph::new(),
            root_node: NodeIndex::new(0),
            state_mapping: bimap::BiMap::new(),
            action_mapping: HashMap::new(),
            phantom: std::marker::PhantomData,
        }
    }

    fn get_or_create_state(&mut self, game_state: &GameState<Game::SpecificState>, player_pov: PlayerId) -> NodeIndex {
        match self.state_mapping.get_by_right(game_state) {
            Some(node_id) => *node_id,
            None => {
                let new_node = self.graph.add_node(MCTSState::new(player_pov));
                self.state_mapping.insert(new_node, game_state.clone());
                new_node
            },
        }
    }

    fn select(&self) -> NodeIndex {
        let mut current_node = self.root_node;
        let mut simulation_count;

        loop {
            if let Some(node) = self.graph.node_weight(current_node) {
                simulation_count = node.play_count;
            } else {
                panic!("Unknown node: {:#?}", current_node);
            }
            match self.graph.neighbors(current_node).max_by(|node_a, node_b| {
                let weight_a = self.graph.node_weight(*node_a).unwrap();
                let ucb_a = weight_a.compute_ucb(simulation_count, self.exploration_factor);

                let weight_b = self.graph.node_weight(*node_b).unwrap();
                let ucb_b = weight_b.compute_ucb(simulation_count, self.exploration_factor);

                return ucb_a.partial_cmp(&ucb_b).unwrap_or(Ordering::Equal);
            }) {
                Some(node) => { current_node = node; },
                None => { break; },
            }
        }
        return current_node;
    }

    fn expand(&mut self, node: &NodeIndex) -> NodeIndex {
        let game_state = self.state_mapping.get_by_left(node).unwrap().clone();
        let status = self.game.compute_status(&game_state);
        if status != GameStatus::Ongoing {
            return *node;
        }
        let mut new_nodes = Vec::new();
        for action in MoveIterator::new(&self.game, &game_state) {
            let resulting_state = self.game.play_action(&game_state, &action);
            let resulting_node = self.get_or_create_state(&resulting_state, game_state.current_player);
            let edge_id = self.graph.add_edge(*node, resulting_node, ());
            self.action_mapping.insert(edge_id, action);
            new_nodes.push(resulting_node);
        }
        let mut rng = rand::thread_rng();
        if let Some(node_id) = new_nodes.choose(&mut rng) {
            return *node_id;
        } else {
            panic!("No successors");
        }
    }

    fn simulate(&self, node_id: &NodeIndex) -> GameStatus {
        let mut game_state = self.state_mapping.get_by_left(&node_id).unwrap().clone();
        let mut status = self.game.compute_status(&game_state);
        let mut rng = rand::thread_rng();

        while status == GameStatus::Ongoing {
            let possible_actions: Vec<MoveIterator::Item> = MoveIterator::new(&self.game, &game_state).collect();
            if let Some(chosen_action) = possible_actions.choose(&mut rng) {
                game_state = self.game.play_action(&game_state, &chosen_action);
                status = self.game.compute_status(&game_state);
            } else {
                panic!("Ongoing game, and no available actions");
            }
        }
        return status;
    }

    fn backpropagate(&mut self, node: &NodeIndex, status: &GameStatus) {
        let mut todo = Vec::new();
        let mut seen = HashSet::new();
        todo.push(*node);
        while let Some(node_id) = todo.pop() {
            if seen.contains(&node_id) {
                continue;
            }
            let weight = self.graph.node_weight_mut(node_id).unwrap();
            weight.update(&status);
            let parents = self.graph.neighbors_directed(node_id, petgraph::Direction::Incoming);
            todo.extend(parents);
            seen.insert(node_id);
        }
    }

    fn find_best_continuation(&self, node_id: NodeIndex) -> Vec<Game::Action> {
        let mut current_node = Some(node_id);
        let mut result = Vec::new();
        while let Some(node_id) = current_node {
            let best_continuation = self.graph.neighbors(node_id).max_by(|node_a, node_b| {
                let weight_a = self.graph.node_weight(*node_a).unwrap();
                let value_a = weight_a.compute_value(None);

                let weight_b = self.graph.node_weight(*node_b).unwrap();
                let value_b = weight_b.compute_value(None);

                value_a.partial_cmp(&value_b).unwrap_or(Ordering::Equal)
            });
            if let Some(next_node_id) = best_continuation {
                let edge = self.graph.find_edge(node_id, next_node_id).unwrap();
                let action = &self.action_mapping[&edge];
                result.push(action.clone());
            }
            current_node = best_continuation;
        }
        return result;
    }
}


impl<Game, MoveIterator> Policy<Game, MoveIterator> for MCTS<Game, MoveIterator> where
    Game: GameRules,
    MoveIterator: ActionIterator<Game>,
{
    fn set_state(&mut self, state: GameState<Game::SpecificState>, player_pov: PlayerId) {
        self.root_node = self.get_or_create_state(&state, player_pov);
    }

    fn evaluate(&self, player_pov: PlayerId, limit: usize) -> Vec<ActionEvaluation<Game::Action>> {
        let mut result: Vec<ActionEvaluation<Game::Action>>;
        result = self.graph.neighbors(self.root_node).map(|node_id| {
            let weight = self.graph.node_weight(node_id).unwrap();
            let edge_id = self.graph.find_edge(self.root_node, node_id).unwrap();
            let action = &self.action_mapping[&edge_id];
            let value = weight.compute_value(Some(player_pov));
            let continuation = self.find_best_continuation(node_id);
            return ActionEvaluation{
                action: action.clone(),
                continuation: continuation,
                score: value,
            };
        }).filter(
            |evaluation| !evaluation.score.is_nan()
        ).collect();
        result.sort_unstable_by(|eval_a, eval_b| {
            match eval_a.score.partial_cmp(&eval_b.score) {
                Some(Ordering::Less) => Ordering::Greater,
                Some(Ordering::Greater) => Ordering::Less,
                _ => Ordering::Equal,
            }
        });
        result.truncate(limit);
        result
    }

    fn iterate(&mut self) {
        let selected_node = self.select();
        let expanded_node = self.expand(&selected_node);
        let simulation_status = self.simulate(&expanded_node);
        self.backpropagate(&expanded_node, &simulation_status);
    }
}
