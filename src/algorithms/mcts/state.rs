use crate::games::{GameStatus, PlayerId};


#[derive(Debug)]
pub struct MCTSState {
    pub play_count: f32,
    win_count: f32,
    player_pov: PlayerId,
}


impl MCTSState {
    pub fn new(player_pov: PlayerId) -> MCTSState {
        MCTSState {
            play_count: 0.,
            win_count: 0.,
            player_pov: player_pov,
        }
    }

    pub fn compute_value(&self, player: Option<PlayerId>) -> f32 {
        let factor = match player {
            Some(player_id) => if player_id == self.player_pov { 1.0 } else { -1.0 },
            None => 1.0,
        };
        factor * self.win_count / self.play_count
    }

    pub fn compute_ucb(&self, simulation_count: f32, exploration_factor: f32) -> f32 {
        if self.play_count == 0. {
            return std::f32::INFINITY;
        }
        let mut result = 2. * simulation_count.ln() / self.play_count;
        result = result.sqrt() * exploration_factor;
        result += self.compute_value(None);
        return result;
    }
    
    pub fn update(&mut self, status: &GameStatus) {
        self.play_count += 1.;
        self.win_count += match status {
            GameStatus::Draw => 0.,
            GameStatus::Winner(winner) => {
                if *winner == self.player_pov { 1. } else { -1. }
            },
            _ => { panic!("Non-finished state sent"); },
        }
    }
}
