use crate::games::{ActionIterator, GameRules, GameState, PlayerId};


#[derive(Debug, Serialize)]
pub struct ActionEvaluation<Action> where
    Action: serde::Serialize,
{
    pub action: Action,
    pub score: f32,
    pub continuation: Vec<Action>,
}


pub trait Policy<Game, MoveIterator> where
    Game: GameRules,
    MoveIterator: ActionIterator<Game>,
{
    fn set_state(&mut self, state: GameState<Game::SpecificState>, player_pov: PlayerId);
    fn evaluate(&self, player_pov: PlayerId, limit: usize) -> Vec<ActionEvaluation<Game::Action>>;
    fn iterate(&mut self);
}
